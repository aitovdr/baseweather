# BaseWeather

Weather, everybody wants to know how it is going to be during the week. Will it be rainy, windy, or sunny? Luckily for us, in the information age, there are open APIs to retrieve information about it. This project uses openweathermap.org to retrieve the weather information and show it on a screen of an iOS device.

The application contains the following screens:

* Home screen:
    * Showing a list of locations that the user has bookmarked previously.
    * Show a way to remove locations from the list
    * Add locations by placing a pin on the map.
* City screen: 
    * Today’s forecast, including temperature, humidity, rain chances and wind information
* Help screen: