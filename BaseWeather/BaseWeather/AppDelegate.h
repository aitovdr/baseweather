//
//  AppDelegate.h
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 30/05/2017.
//  Copyright © 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

