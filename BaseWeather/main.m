//
//  main.m
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 30/05/2017.
//  Copyright © 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
