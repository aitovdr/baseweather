//
//  ADRBWHelpScreenViewController.m
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 10/06/2017.
//  Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWHelpScreenViewController.h"

@interface ADRBWHelpScreenViewController ()

@property (nonatomic, strong, readonly, nonnull) UIStackView *containerStackView;
@property (nonatomic, strong, readonly, nonnull) UINavigationBar *navigationBar;
@property (nonatomic, strong, readonly, nonnull) UIWebView *webView;

@end

@implementation ADRBWHelpScreenViewController

- (void)dealloc
{
    [self teardownObservers];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupSubviews];
    [self setupConstraints];
    [self setupAccessibilityIdentifiers];
    [self setupObservers];
    [self updateSubviews];
}

- (void)setupSubviews
{
    self.view.backgroundColor = UIColor.whiteColor;

    _containerStackView = [[UIStackView alloc] init];
    self.containerStackView.axis = UILayoutConstraintAxisVertical;
    self.containerStackView.distribution = UIStackViewDistributionFill;
    self.containerStackView.alignment = UIStackViewAlignmentLeading;
    self.containerStackView.spacing = 0.5;
    self.containerStackView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.containerStackView];

    _navigationBar = [[UINavigationBar alloc] init];
    UINavigationItem *navigationBarItem = [[UINavigationItem alloc] init];
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:@"▼"
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:@selector(dismiss)];

    [button setTitleTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:25.f],
                    NSForegroundColorAttributeName: [UIColor colorWithRed:21.0f/255.0f green:126.0f/255.0f blue:251.0f/255.0f alpha:1.0f]}
                          forState:UIControlStateNormal];

    navigationBarItem.leftBarButtonItem = button;

    [self.navigationBar pushNavigationItem:navigationBarItem animated:NO];
    self.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationBar.translucent = NO;

    [self.containerStackView addArrangedSubview:self.navigationBar];

    _webView = [[UIWebView alloc] init];
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"BaseWeatherUserManual" ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [self.webView loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];

    [self.containerStackView addArrangedSubview:self.webView];
}

- (void)setupConstraints
{
    [self.containerStackView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [self.containerStackView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.containerStackView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;

    [self.navigationBar.heightAnchor constraintEqualToConstant:64.0f].active = YES;
    [self.navigationBar.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.navigationBar.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;

    [self.webView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    [self.webView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.webView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
}

- (void)setupAccessibilityIdentifiers
{

}

- (void)updateSubviews
{

}

- (void)setupObservers
{

}

- (void)teardownObservers
{

}

- (void)dismiss
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{

}

@end
