//
// Created by dmitrii.aitov@philips.com on 04/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol ADRBWLocationCellViewModelProtocol;

@interface ADRBWLocationCell : UICollectionViewCell

@property (nonatomic, strong, readwrite, nonnull) NSObject<ADRBWLocationCellViewModelProtocol> *viewModel;

@property (nonatomic, assign, readwrite) BOOL editing;

@end