//
//  ADRBWHomeScreenViewController.h
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 02/06/2017.
//  Copyright © 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ADRBWHomeScreenViewModelProtocol;

@interface ADRBWHomeScreenViewController : UIViewController

- (nullable instancetype)initWithViewModel:(nonnull NSObject<ADRBWHomeScreenViewModelProtocol> *)viewModel;

///Made init and new unavailable to avoid creation without mandatory parameters
+ (nullable instancetype)new NS_UNAVAILABLE;
- (nullable instancetype)init NS_UNAVAILABLE;

@end

