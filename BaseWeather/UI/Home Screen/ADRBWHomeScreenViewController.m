//
//  ADRBWHomeScreenViewController.m
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 02/06/2017.
//  Copyright © 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWHomeScreenViewController.h"
#import "ADRBWHomeScreenViewModelProtocol.h"
#import "ADRBWLocationCell.h"
#import "ADRBWLocationScreenViewController.h"
#import "ADRBWHelpScreenViewController.h"
#import "ADRBWErrorDelegate.h"
#import "ADRBWError.h"
#import "ADRBWLocationCellViewModelProtocol.h"
#import <MapKit/MapKit.h>

static NSString * const kLocationCellReuseIdentifier = @"kLocationCellReuseIdentifier";

@interface ADRBWHomeScreenViewController () <MKMapViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, ADRBWErrorDelegate>

@property (nonatomic, strong, readonly, nonnull) NSObject<ADRBWHomeScreenViewModelProtocol> *viewModel;

@property (nonatomic, strong, readonly, nonnull) UIStackView *containerStackView;
@property (nonatomic, strong, readonly, nonnull) UICollectionView *locationsCollectionView;
@property (nonatomic, strong, readonly, nonnull) MKMapView *mapView;
@property (nonatomic, strong, readonly, nonnull) UIButton *addLocationButton;

@property (nonatomic, strong, nonnull) MKPointAnnotation *centerAnnotaion;
@property (nonatomic, strong, nonnull) MKPinAnnotationView *centerAnnotationView;

@property (nonatomic, strong) NSTimer *updateLocationTimer;

@end

@implementation ADRBWHomeScreenViewController

- (nullable instancetype)initWithViewModel:(nonnull NSObject<ADRBWHomeScreenViewModelProtocol> *)viewModel
{
    self = [super init];

    if (self)
    {
        _viewModel = viewModel;
        _viewModel.errorDelegate = self;
    }

    return self;
}

- (void)dealloc
{
    [self teardownObservers];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupSubviews];
    [self setupConstraints];
    [self setupAccessibilityIdentifiers];
    [self setupObservers];
}

- (void)setupSubviews
{
    self.view.backgroundColor = [UIColor colorWithRed:176.0f/255.0f green:225.0f/255.0f blue:244.0f/255.0f alpha:1.0f];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(helpButtonHandler)];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self setToolbarItems:@[
            [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
            [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteButtonHandler)],
            [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]
    ]];

    _containerStackView = [[UIStackView alloc] init];
    self.containerStackView.axis = UILayoutConstraintAxisVertical;
    self.containerStackView.distribution = UIStackViewDistributionFill;
    self.containerStackView.alignment = UIStackViewAlignmentLeading;
    self.containerStackView.spacing = 0;
    self.containerStackView.translatesAutoresizingMaskIntoConstraints = NO;

    [self.view addSubview:self.containerStackView];

    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(100 , 130);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumInteritemSpacing = 20.0f;
    flowLayout.minimumLineSpacing = 20.0f;
    _locationsCollectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:flowLayout];
    self.locationsCollectionView.delegate = self;
    self.locationsCollectionView.dataSource = self;
    [self.locationsCollectionView registerClass:[ADRBWLocationCell class] forCellWithReuseIdentifier:kLocationCellReuseIdentifier];
    self.locationsCollectionView.translatesAutoresizingMaskIntoConstraints = NO;
    self.locationsCollectionView.backgroundColor = UIColor.clearColor;
    self.locationsCollectionView.contentInset = UIEdgeInsetsMake(15, 15, 15, 15);

    [self.containerStackView addArrangedSubview:self.locationsCollectionView];

    _mapView = [[MKMapView alloc] init];
    self.mapView.delegate = self;
    self.mapView.translatesAutoresizingMaskIntoConstraints = NO;
    self.mapView.layer.borderColor = UIColor.blackColor.CGColor;
    self.mapView.layer.borderWidth = 0.5f;


    [self.containerStackView addArrangedSubview:self.mapView];

    _centerAnnotaion = [[MKPointAnnotation alloc] init];
    _centerAnnotationView = [[MKPinAnnotationView alloc] initWithAnnotation:self.centerAnnotaion
                                                            reuseIdentifier:@"centerAnnotationView"];
    self.centerAnnotationView.pinTintColor = [MKPinAnnotationView redPinColor];

    [self.mapView addSubview:self.centerAnnotationView];

    _addLocationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.addLocationButton addTarget:self action:@selector(addLocationHandler) forControlEvents:UIControlEventTouchUpInside];
    [self.addLocationButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.addLocationButton.backgroundColor = [UIColor colorWithRed:66.0f/255.0f green:144.0f/255.0f blue:191.0f/255.0f alpha:1.0f];
    self.addLocationButton.layer.borderColor = [UIColor colorWithRed:21.0f/255.0f green:126.0f/255.0f blue:251.0f/255.0f alpha:1.0f].CGColor;
    self.addLocationButton.layer.borderWidth = 1.0f;
    self.addLocationButton.layer.cornerRadius = 3.0f;
    self.addLocationButton.translatesAutoresizingMaskIntoConstraints = NO;

    [self.view addSubview:self.addLocationButton];
}

- (void)setupConstraints
{
    [self.containerStackView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [self.containerStackView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    [self.containerStackView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.containerStackView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;

    [self.addLocationButton.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.addLocationButton.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-8.0f].active = YES;
    [self.addLocationButton.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:16.0f].active = YES;
    [self.addLocationButton.trailingAnchor constraintEqualToAnchor:self.view.trailingAnchor constant:-16.0f].active = YES;

    [self.locationsCollectionView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.locationsCollectionView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;

    [self.mapView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.mapView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;
    [self.mapView.heightAnchor constraintEqualToAnchor:self.view.heightAnchor multiplier:0.3f].active = YES;
}

- (void)setupAccessibilityIdentifiers
{

}

- (void)updateSubviews
{
    [self.addLocationButton setTitle:self.viewModel.addLocationButtonTitle forState:UIControlStateNormal];
}

- (void)setupObservers
{
    [self.viewModel addObserver:self forKeyPath:NSStringFromSelector(@selector(needUpdateView)) options:NSKeyValueObservingOptionNew context:nil];
}

- (void)teardownObservers
{
    [self.viewModel removeObserver:self forKeyPath:NSStringFromSelector(@selector(needUpdateView))];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (self.viewModel.needUpdateView)
    {
        [self updateSubviews];
        self.viewModel.needUpdateView = NO;
    }
}

- (void)moveMapAnnotationToCoordinate:(CLLocationCoordinate2D) coordinate
{
    CGPoint mapViewPoint = [self.mapView convertCoordinate:coordinate toPointToView:self.mapView];

    // Offset the view from to account for distance from the lower left corner to the pin head
    CGFloat xoffset = CGRectGetMidX(self.centerAnnotationView.bounds) - 7.75f;
    CGFloat yoffset = -CGRectGetMidY(self.centerAnnotationView.bounds) + 5.0f;

    self.centerAnnotationView.center = CGPointMake(mapViewPoint.x + xoffset, mapViewPoint.y + yoffset);
}

- (void)changeRegionToCoordinate:(CLLocationCoordinate2D)coordinate withSize:(NSUInteger)size
{
    MKCoordinateRegion newRegion = MKCoordinateRegionMakeWithDistance(coordinate, size, size);
    [self.mapView setRegion:newRegion animated:YES];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self moveMapAnnotationToCoordinate:self.centerAnnotaion.coordinate];
}

- (void)addLocationHandler
{
    [self.viewModel bookmarkSelectedLocation];
    [self.locationsCollectionView reloadData];
}

- (void)deleteButtonHandler
{
    NSArray<NSIndexPath *> *selectedItems = [self.locationsCollectionView indexPathsForSelectedItems];

    NSMutableIndexSet *selectedItemsIndexes = [[NSMutableIndexSet alloc] init];
    for (NSIndexPath *selectedItem in selectedItems)
    {
        [selectedItemsIndexes addIndex:(NSUInteger)selectedItem.row];
    }
    [self.viewModel removeLocationsAtIndexes:[selectedItemsIndexes copy]];

    [self.locationsCollectionView deleteItemsAtIndexPaths:selectedItems];
}

- (void)helpButtonHandler
{
    ADRBWHelpScreenViewController *helpViewController = [[ADRBWHelpScreenViewController alloc] init];

    [self presentViewController:helpViewController animated:YES completion:nil];
}

- (void)unitButtonHandler
{
    [self.viewModel changeUnit];
    [self.locationsCollectionView reloadData];
}

- (void)updateSelectedCoordinatesHandler
{
    self.viewModel.selectedCoordinate = self.mapView.centerCoordinate;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];

    self.locationsCollectionView.allowsMultipleSelection = editing;
    NSArray<NSIndexPath *> *visibleItems = [self.locationsCollectionView indexPathsForVisibleItems];
    [visibleItems enumerateObjectsUsingBlock:^(NSIndexPath *indexPath, NSUInteger idx, BOOL *stop) {
        [self.locationsCollectionView deselectItemAtIndexPath:indexPath animated:NO];
        ADRBWLocationCell *locationCell = (ADRBWLocationCell *)[self.locationsCollectionView cellForItemAtIndexPath:indexPath];
        locationCell.editing = editing;
    }];

    [self.navigationController setToolbarHidden:!editing animated:animated];

    self.navigationItem.leftBarButtonItem = (editing)
            ? [[UIBarButtonItem alloc] initWithTitle:@"°C/°F" style:UIBarButtonItemStylePlain target:self action:@selector(unitButtonHandler)]
            : [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(helpButtonHandler)];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.viewModel.locations.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ADRBWLocationCell *locationCell = [collectionView dequeueReusableCellWithReuseIdentifier:kLocationCellReuseIdentifier forIndexPath:indexPath];
    return locationCell;
}

#pragma mark <UICollectionViewDelegate>
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSObject<ADRBWLocationCellViewModelProtocol> *locationCellModel = [self.viewModel modelForLocationCellAtIndexPath:indexPath];

    if (locationCellModel)
    {
        ADRBWLocationCell *locationCell = (ADRBWLocationCell *)cell;
        locationCell.viewModel = locationCellModel;
        locationCell.editing = self.isEditing;
        locationCellModel.errorDelegate = self;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.isEditing)
    {
        NSObject<ADRBWLocationScreenViewModelProtocol> *locationScreenViewModel = [self.viewModel modelForLocationScreenAtIndexPath:indexPath];
        ADRBWLocationScreenViewController *locationViewController = [[ADRBWLocationScreenViewController alloc] initWithViewModel:locationScreenViewModel];

        [self presentViewController:locationViewController animated:YES completion:nil];
    }
}

#pragma mark - MapView Delegate methods
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if ((self.centerAnnotaion.coordinate.latitude) != (self.mapView.centerCoordinate.latitude) ||
            (self.centerAnnotaion.coordinate.longitude != (self.mapView.centerCoordinate.longitude)))
    {

        self.centerAnnotaion.coordinate = mapView.centerCoordinate;

        [self moveMapAnnotationToCoordinate:mapView.centerCoordinate];

        [self.updateLocationTimer invalidate];
        self.updateLocationTimer = [NSTimer scheduledTimerWithTimeInterval:0.2
                                         target:self selector:@selector(updateSelectedCoordinatesHandler)
                                       userInfo:nil
                                        repeats:NO];
    }

}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    [self changeRegionToCoordinate:userLocation.coordinate withSize:1000];
}

#pragma mark <ADRBWErrorDelegate>
- (void)didFailOperationWithError:(ADRBWError *)error
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                    message:@"Something goes wrong"
                                                             preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancelAction];

    UIAlertAction *retryAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
       if (error.recoverBlock != nil)
       {
           error.recoverBlock();
       }
    }];
    [alert addAction:retryAction];

    [self presentViewController:alert animated:YES completion:nil];
}

@end
