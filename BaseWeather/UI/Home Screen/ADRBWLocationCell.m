//
// Created by dmitrii.aitov@philips.com on 04/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWLocationCell.h"
#import "ADRBWLocationCellViewModelProtocol.h"


@interface ADRBWLocationCell ()

@property (nonatomic, strong, readonly, nonnull) UIImageView *backgroundImageView;
@property (nonatomic, strong, readonly, nonnull) UILabel *temperatureLabel;
@property (nonatomic, strong, readonly, nonnull) UILabel *nameLabel;
@property (nonatomic, strong, readonly, nonnull) UIImageView *editingImageView;

@end

@implementation ADRBWLocationCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];

    if (self)
    {
        [self setupSubviews];
        [self setupConstraints];
        [self setupAccessibilityIdentifiers];
    }

    return self;
}

- (void)dealloc
{
    [self teardownObservers];
}

- (void)teardownObservers
{
    [self.viewModel removeObserver:self forKeyPath:NSStringFromSelector(@selector(needUpdateView))];
}

- (void)setupObservers
{
    [self.viewModel addObserver:self forKeyPath:NSStringFromSelector(@selector(needUpdateView)) options:NSKeyValueObservingOptionNew context:nil];
}

- (void)setupSubviews
{
    self.backgroundColor = [UIColor colorWithRed:66.0f/255.0f green:144.0f/255.0f blue:190.0f/255.0f alpha:1.0f];
    self.layer.borderColor = [UIColor colorWithRed:21.0f/255.0f green:126.0f/255.0f blue:251.0f/255.0f alpha:1.0f].CGColor;
    self.layer.borderWidth = 1.0f;
    self.layer.cornerRadius = 3.0f;
    self.layer.masksToBounds = YES;

    _backgroundImageView = [[UIImageView alloc] init];
    self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:self.backgroundImageView];

    _editingImageView = [[UIImageView alloc] init];
    self.editingImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.editingImageView.image = [UIImage imageNamed:@"unchecked"];
    self.editingImageView.hidden = YES;
    [self addSubview:self.editingImageView];

    _temperatureLabel = [[UILabel alloc] init];
    self.temperatureLabel.textColor = UIColor.whiteColor;
    self.temperatureLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.temperatureLabel.font = [UIFont systemFontOfSize:40.0f weight:UIFontWeightUltraLight];
    self.temperatureLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.temperatureLabel];

    _nameLabel = [[UILabel alloc] init];
    self.nameLabel.textColor = UIColor.whiteColor;
    self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.nameLabel.numberOfLines = 0;
    self.nameLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.nameLabel];
}

- (void)setupConstraints
{
    [self.backgroundImageView.topAnchor constraintEqualToAnchor:self.topAnchor].active = YES;
    [self.backgroundImageView.bottomAnchor constraintEqualToAnchor:self.bottomAnchor].active = YES;
    [self.backgroundImageView.leftAnchor constraintEqualToAnchor:self.leftAnchor].active = YES;
    [self.backgroundImageView.rightAnchor constraintEqualToAnchor:self.rightAnchor].active = YES;

    [self.editingImageView.topAnchor constraintEqualToAnchor:self.topAnchor constant:3.0f].active = YES;
    [self.editingImageView.rightAnchor constraintEqualToAnchor:self.rightAnchor constant:-3.0f].active = YES;

    [self.temperatureLabel.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = YES;
    [self.temperatureLabel.bottomAnchor constraintEqualToAnchor:self.centerYAnchor].active = YES;

    [self.nameLabel.centerXAnchor constraintEqualToAnchor:self.centerXAnchor].active = YES;
    [self.nameLabel.topAnchor constraintEqualToAnchor:self.temperatureLabel.bottomAnchor constant:16.0f].active = YES;
    [self.nameLabel.leadingAnchor constraintEqualToAnchor:self.leadingAnchor constant:8].active = YES;
    [self.nameLabel.trailingAnchor constraintEqualToAnchor:self.trailingAnchor constant:-8].active = YES;
}

- (void)setupAccessibilityIdentifiers
{

}

- (void)updateSubviews
{
    self.backgroundImageView.image = [UIImage imageNamed:self.viewModel.backgroundImageName];
    self.temperatureLabel.text = self.viewModel.temperature;
    self.nameLabel.text = self.viewModel.name;
}

- (void)setViewModel:(NSObject <ADRBWLocationCellViewModelProtocol> *)viewModel
{
    [self teardownObservers];

    _viewModel = viewModel;

    [self setupObservers];

    [self updateSubviews];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (self.viewModel.needUpdateView)
    {
        [self updateSubviews];
        self.viewModel.needUpdateView = NO;
    }
}

- (void)setEditing:(BOOL)editing
{
    _editing = editing;
    self.editingImageView.hidden = !editing;
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];

    if (self.editing)
    {
        self.editingImageView.image = [UIImage imageNamed:selected ? @"checked" : @"unchecked"];
    }
}

@end