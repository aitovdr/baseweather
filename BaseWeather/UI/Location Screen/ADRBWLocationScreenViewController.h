//
//  ADRBWLocationScreenViewController.h
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 05/06/2017.
//  Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ADRBWLocationScreenViewModelProtocol;


@interface ADRBWLocationScreenViewController : UIViewController

- (nullable instancetype)initWithViewModel:(nonnull NSObject<ADRBWLocationScreenViewModelProtocol> *)viewModel;

///Made init and new unavailable to avoid creation without mandatory parameters
+ (nullable instancetype)new NS_UNAVAILABLE;
- (nullable instancetype)init NS_UNAVAILABLE;

@end
