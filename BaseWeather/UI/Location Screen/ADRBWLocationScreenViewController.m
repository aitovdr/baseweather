//
//  ADRBWLocationScreenViewController.m
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 05/06/2017.
//  Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWLocationScreenViewController.h"
#import "ADRBWLocationScreenViewModelProtocol.h"
#import "ADRBWError.h"
#import "ADRBWErrorDelegate.h"

@interface ADRBWLocationScreenViewController () <ADRBWErrorDelegate>

@property (nonatomic, strong, readonly, nonnull) NSObject<ADRBWLocationScreenViewModelProtocol> *viewModel;

@property (nonatomic, strong, readonly, nonnull) UIStackView *containerStackView;
@property (nonatomic, strong, readonly, nonnull) UINavigationBar *navigationBar;
@property (nonatomic, strong, readonly, nonnull) UIImageView *backgroundImageView;
@property (nonatomic, strong, readonly, nonnull) UILabel *temperatureLabel;
@property (nonatomic, strong, readonly, nonnull) UILabel *nameLabel;
@property (nonatomic, strong, readonly, nonnull) UILabel *conditionLabel;
@property (nonatomic, strong, readonly, nonnull) UILabel *humidityLabel;
@property (nonatomic, strong, readonly, nonnull) UILabel *rainLabel;
@property (nonatomic, strong, readonly, nonnull) UILabel *windLabel;

@end

@implementation ADRBWLocationScreenViewController

- (instancetype)initWithViewModel:(NSObject <ADRBWLocationScreenViewModelProtocol> *)viewModel
{
    self = [super init];

    if (self)
    {
        _viewModel = viewModel;
        _viewModel.errorDelegate = self;
    }

    return self;
}

- (void)dealloc
{
    [self teardownObservers];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupSubviews];
    [self setupConstraints];
    [self setupAccessibilityIdentifiers];
    [self setupObservers];
    [self updateSubviews];
}

- (void)setupSubviews
{
    self.view.backgroundColor = UIColor.greenColor;

    _backgroundImageView = [[UIImageView alloc] init];
    self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.backgroundImageView];

    _containerStackView = [[UIStackView alloc] init];
    self.containerStackView.axis = UILayoutConstraintAxisVertical;
    self.containerStackView.distribution = UIStackViewDistributionFill;
    self.containerStackView.alignment = UIStackViewAlignmentLeading;
    self.containerStackView.spacing = 0;
    self.containerStackView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.containerStackView];

    _navigationBar = [[UINavigationBar alloc] init];
    UINavigationItem *navigationBarItem = [[UINavigationItem alloc] init];
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithTitle:@"▼"
                                                               style:UIBarButtonItemStylePlain
                                                              target:self
                                                              action:@selector(dismiss)];

    [button setTitleTextAttributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:25.f],
                    NSForegroundColorAttributeName : [UIColor colorWithRed:21.0f/255.0f green:126.0f/255.0f blue:251.0f/255.0f alpha:1.0f]}
                          forState:UIControlStateNormal];

    navigationBarItem.leftBarButtonItem = button;

    [self.navigationBar pushNavigationItem:navigationBarItem animated:NO];
    self.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationBar.translucent = NO;

    [self.containerStackView addArrangedSubview:self.navigationBar];

    _nameLabel = [[UILabel alloc] init];
    self.nameLabel.textColor = UIColor.whiteColor;
    self.nameLabel.font = [UIFont boldSystemFontOfSize:22.0f];
    self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.nameLabel.numberOfLines = 0;
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    [self.containerStackView addArrangedSubview:self.nameLabel];

    _conditionLabel = [[UILabel alloc] init];
    self.conditionLabel.textColor = UIColor.whiteColor;
    self.conditionLabel.font = [UIFont systemFontOfSize:14.0f];
    self.conditionLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.conditionLabel.numberOfLines = 0;
    self.conditionLabel.textAlignment = NSTextAlignmentCenter;
    [self.containerStackView addArrangedSubview:self.conditionLabel];

    _temperatureLabel = [[UILabel alloc] init];
    self.temperatureLabel.textColor = UIColor.whiteColor;
    self.temperatureLabel.font = [UIFont systemFontOfSize:90.0f weight:UIFontWeightUltraLight];
    self.temperatureLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.temperatureLabel.textAlignment = NSTextAlignmentCenter;
    [self.containerStackView addArrangedSubview:self.temperatureLabel];

    _humidityLabel = [[UILabel alloc] init];
    self.humidityLabel.textColor = UIColor.whiteColor;
    self.humidityLabel.font = [UIFont systemFontOfSize:16.0f];
    self.humidityLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.humidityLabel.numberOfLines = 0;
    self.humidityLabel.textAlignment = NSTextAlignmentCenter;
    [self.containerStackView addArrangedSubview:self.humidityLabel];

    _rainLabel = [[UILabel alloc] init];
    self.rainLabel.textColor = UIColor.whiteColor;
    self.rainLabel.font = [UIFont systemFontOfSize:16.0f];
    self.rainLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.rainLabel.numberOfLines = 0;
    self.rainLabel.textAlignment = NSTextAlignmentCenter;
    [self.containerStackView addArrangedSubview:self.rainLabel];

    _windLabel = [[UILabel alloc] init];
    self.windLabel.textColor = UIColor.whiteColor;
    self.windLabel.font = [UIFont systemFontOfSize:16.0f];
    self.windLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.windLabel.numberOfLines = 0;
    self.windLabel.textAlignment = NSTextAlignmentCenter;
    [self.containerStackView addArrangedSubview:self.windLabel];
}

- (void)setupConstraints
{
    [self.backgroundImageView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    [self.backgroundImageView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    [self.backgroundImageView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.backgroundImageView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;

    [self.containerStackView.topAnchor constraintEqualToAnchor:self.view.topAnchor].active = YES;
    //[self.containerStackView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor].active = YES;
    [self.containerStackView.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.containerStackView.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;

    [self.navigationBar.heightAnchor constraintEqualToConstant:64.0f].active = YES;
    [self.navigationBar.leftAnchor constraintEqualToAnchor:self.view.leftAnchor].active = YES;
    [self.navigationBar.rightAnchor constraintEqualToAnchor:self.view.rightAnchor].active = YES;

    [self.temperatureLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;

    [self.nameLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
    [self.nameLabel.heightAnchor constraintEqualToConstant:64.0f].active = YES;

    [self.conditionLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;

    [self.humidityLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;

    [self.rainLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;

    [self.windLabel.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor].active = YES;
}

- (void)setupAccessibilityIdentifiers
{

}

- (void)updateSubviews
{
    self.backgroundImageView.image = [UIImage imageNamed:self.viewModel.backgroundImageName];
    self.temperatureLabel.text = self.viewModel.temperature;
    self.nameLabel.text = self.viewModel.name;
    self.conditionLabel.text = self.viewModel.condition;
    self.humidityLabel.text = self.viewModel.humidity;
    self.rainLabel.text = self.viewModel.rain;
    self.windLabel.text = self.viewModel.wind;
}

- (void)setupObservers
{
    [self.viewModel addObserver:self forKeyPath:NSStringFromSelector(@selector(needUpdateView)) options:NSKeyValueObservingOptionNew context:nil];
}

- (void)teardownObservers
{
    [self.viewModel removeObserver:self forKeyPath:NSStringFromSelector(@selector(needUpdateView))];
}

- (void)dismiss
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (self.viewModel.needUpdateView)
    {
        [self updateSubviews];
        self.viewModel.needUpdateView = NO;
    }
}

#pragma mark <ADRBWErrorDelegate>
- (void)didFailOperationWithError:(ADRBWError *)error
{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                    message:@"Something goes wrong"
                                                             preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancelAction];

    UIAlertAction *retryAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if (error.recoverBlock != nil)
        {
            error.recoverBlock();
        }
    }];
    [alert addAction:retryAction];

    [self presentViewController:alert animated:YES completion:nil];
}

@end
