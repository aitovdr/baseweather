//
// Created by dmitrii.aitov@philips.com on 03/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADRBWWeather;


@interface ADRBWLocation : NSObject <NSCoding>

@property (nonatomic, copy, readonly, nonnull) NSString *name;
@property (nonatomic, copy, readonly, nonnull) NSString *country;
@property (nonatomic, assign, readonly) NSUInteger identifier;

@property (nonatomic, copy, readonly, nonnull) NSDictionary<NSDate *, ADRBWWeather *> *weathers;

- (nullable instancetype)initWithName:(nonnull NSString *)name
                              country:(nonnull NSString *)country
                           identifier:(NSUInteger)identifier;

- (nullable ADRBWWeather *)currentWeather;
- (void)setWeather:(nonnull ADRBWWeather *)weather toDate:(nonnull NSDate *)date;
- (nullable ADRBWWeather *)weatherByDate:(nonnull NSDate *)date;

///Made init and new unavailable to avoid creation without mandatory parameters
+ (nullable instancetype)new NS_UNAVAILABLE;
- (nullable instancetype)init NS_UNAVAILABLE;

@end
