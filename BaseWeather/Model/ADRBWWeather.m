//
// Created by dmitrii.aitov@philips.com on 03/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWWeather.h"


@implementation ADRBWWeather

- (instancetype)initWithTemperature:(double)temperature
                     temperatureMin:(double)temperatureMin
                     temperatureMax:(double)temperatureMax
                           humidity:(NSUInteger)humidity
                           pressure:(double)pressure
                               wind:(double)wind
                      windDirection:(NSUInteger)windDirection
                         rainVolume:(double)rainVolume
                          condition:(NSString *)condition
                               type:(ADRBWWeatherType)type
{
    self = [super init];

    if (self)
    {
        _temperature = temperature;
        _temperatureMin = temperatureMin;
        _temperatureMax = temperatureMax;
        _humidity = humidity;
        _pressure = pressure;
        _wind = wind;
        _windDirection = windDirection;
        _rainVolume = rainVolume;
        _condition = [condition copy];
        _type = type;
    }

    return self;
}


@end