//
// Created by dmitrii.aitov@philips.com on 03/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ADRBWWeatherType)
{
    ADRBWWeatherTypeClearSky,
    ADRBWWeatherTypeFewClouds,
    ADRBWWeatherTypeScatteredClouds,
    ADRBWWeatherTypeBrokenClouds,
    ADRBWWeatherTypeShowerRain,
    ADRBWWeatherTypeRain,
    ADRBWWeatherTypeThunderstorm,
    ADRBWWeatherTypeSnow,
    ADRBWWeatherTypeMist
};

@interface ADRBWWeather : NSObject

@property (nonatomic, assign, readonly) double temperature;
@property (nonatomic, assign, readonly) double temperatureMin;
@property (nonatomic, assign, readonly) double temperatureMax;
@property (nonatomic, assign, readonly) NSUInteger humidity;
@property (nonatomic, assign, readonly) double pressure;
@property (nonatomic, assign, readonly) double wind;
@property (nonatomic, assign, readonly) NSUInteger windDirection;
@property (nonatomic, assign, readonly) double rainVolume;

@property (nonatomic, copy, readonly, nonnull) NSString *condition;
@property (nonatomic, assign, readonly) ADRBWWeatherType type;

- (nullable instancetype)initWithTemperature:(double)temperature
                     temperatureMin:(double)temperatureMin
                     temperatureMax:(double)temperatureMax
                           humidity:(NSUInteger)humidity
                           pressure:(double)pressure
                               wind:(double)wind
                      windDirection:(NSUInteger)windDirection
                         rainVolume:(double)rainVolume
                          condition:(nonnull NSString *)condition
                               type:(ADRBWWeatherType)type;

///Made init and new unavailable to avoid creation without mandatory parameters
+ (nullable instancetype)new NS_UNAVAILABLE;
- (nullable instancetype)init NS_UNAVAILABLE;

@end