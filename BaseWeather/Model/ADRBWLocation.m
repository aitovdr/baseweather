//
// Created by dmitrii.aitov@philips.com on 03/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWLocation.h"
#import "ADRBWWeather.h"
#import "ADRBWLocationScreenViewModelProtocol.h"
#import "NSDate+Utils.h"
#import "ADRBWErrorDelegate.h"

@interface ADRBWLocation ()

@property (nonatomic, copy, readwrite, nonnull) NSDictionary<NSDate *, ADRBWWeather *> *weathers;

@end

@implementation ADRBWLocation

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super init];

    if (self)
    {
        _name = [coder decodeObjectForKey:@"kNameKey"];
        _country = [coder decodeObjectForKey:@"kCountryKey"];
        _identifier = (NSUInteger)[coder decodeIntegerForKey:@"kIdentifierKey"];

        _weathers = [NSDictionary dictionary];
    }

    return self;
}

- (instancetype)initWithName:(nonnull NSString *)name country:(nonnull NSString *)country identifier:(NSUInteger)identifier
{
    self = [super init];

    if (self)
    {
        _name = name;
        _country = country;
        _identifier = identifier;

        _weathers = [NSMutableDictionary dictionary];
    }

    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.name forKey:@"kNameKey"];
    [coder encodeObject:self.country forKey:@"kCountryKey"];
    [coder encodeInteger:self.identifier forKey:@"kIdentifierKey"];
}

- (nullable ADRBWWeather *)currentWeather
{
    return self.weathers[[[NSDate date] dateAtStartOfDay]];
}

- (void)setWeather:(nonnull ADRBWWeather *)weather toDate:(nonnull NSDate *)date
{
    NSMutableDictionary *mutableWeathers = [self.weathers mutableCopy];
    mutableWeathers[[date dateAtStartOfDay]] = weather;
    self.weathers = mutableWeathers;
}

- (nullable ADRBWWeather *)weatherByDate:(nonnull NSDate *)date
{
    return self.weathers[[date dateAtStartOfDay]];
}

- (BOOL)isEqualToLocation:(ADRBWLocation *)location
{
    if (!location)
    {
        return NO;
    }

    return self.identifier == location.identifier;
}

- (BOOL)isEqual:(id)object
{
    if (self == object)
    {
        return YES;
    }

    if (![object isKindOfClass:[ADRBWLocation class]])
    {
        return NO;
    }

    return [self isEqualToLocation:(ADRBWLocation *)object];
}

- (NSUInteger)hash
{
    return [self.name hash] ^ self.identifier;
}

@end