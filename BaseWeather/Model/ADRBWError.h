//
// Created by dmitrii.aitov@philips.com on 11/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ADRBWErrorRecoverBlock)();

@interface ADRBWError : NSError

@property (nonatomic, copy, readwrite, nullable) ADRBWErrorRecoverBlock recoverBlock;

@end