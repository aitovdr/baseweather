//
// Created by dmitrii.aitov@philips.com on 01/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWCurrentWeatherResource.h"
#import "ADRNetworkClientProtocol.h"
#import "ADRNetworkService.h"


@implementation ADRBWCurrentWeatherResource

- (NSURLSessionTask *)getCurrentWeatherByLocationIdentifier:(NSUInteger)identifier completion:(ADRNetworkClientCompletionBlock)completion
{
    NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:[NSURL URLWithString:@"weather" relativeToURL:self.networkService.baseURL] resolvingAgainstBaseURL:YES];
    urlComponents.queryItems = @[[NSURLQueryItem queryItemWithName:@"id" value:[NSString stringWithFormat:@"%lu", identifier]]];

    [self.networkService addParametersToURLComponents:urlComponents];

    NSURLRequest *request = [NSURLRequest requestWithURL:urlComponents.URL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10 * 1000];

    return [self.networkService.networkCLient executeRequest:request completion:completion];
}

@end
