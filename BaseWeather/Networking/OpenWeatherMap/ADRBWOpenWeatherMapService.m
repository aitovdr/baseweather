//
// Created by dmitrii.aitov@philips.com on 01/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWOpenWeatherMapService.h"
#import "ADRNetworkClientProtocol.h"
#import "ADRBWFindLocationResource.h"
#import "ADRBWCurrentWeatherResource.h"
#import "ADRBWForecastWeatherResource.h"

static NSString *const kApiKeyQueryKey = @"appid";
static NSString *const kFormatQueryKey = @"mode";
static NSString *const kLanguageQueryKey = @"lang";
static NSString *const kUnitsQueryKey = @"units";

static NSString *const kLanguageQueryValue = @"en";
static NSString *const kFormatQueryValue = @"json";
static NSString *const kUnitsQueryValue = @"metric";

static NSString *const kOpenWeatherMapRestAPIBaseUrl = @"http://api.openweathermap.org/data/2.5/";

///Base url to flickr.com REST API
static NSURL *kOpenWeatherMapBaseURL = nil;


@interface ADRBWOpenWeatherMapService ()

@property (nonatomic, copy, readonly, nonnull) NSString *authorizationKey;

@end

@implementation ADRBWOpenWeatherMapService
@synthesize findLocationResource = _findLocationResource;
@synthesize currentWeatherResource = _currentWeatherResource;
@synthesize forecastWeatherResource = _forecastWeatherResource;

+ (void)initialize
{
    if (!kOpenWeatherMapBaseURL)
    {
        kOpenWeatherMapBaseURL = [[NSURL alloc] initWithString:kOpenWeatherMapRestAPIBaseUrl];
    }
}

- (nullable instancetype)initWithAuthorizationKey:(nonnull NSString *)authorizationKey
                                    networkClient:(nonnull NSObject<ADRNetworkClientProtocol> *)networkClient
{
    self = [super initWithBaseURL:kOpenWeatherMapBaseURL networkClient:networkClient];

    if (self)
    {
        _authorizationKey = authorizationKey;
    }

    return self;
}

- (void)addParametersToURLComponents:(NSURLComponents *)urlComponents
{
    urlComponents.queryItems = [urlComponents.queryItems arrayByAddingObject:[NSURLQueryItem queryItemWithName:kFormatQueryKey value:kFormatQueryValue]];
    urlComponents.queryItems = [urlComponents.queryItems arrayByAddingObject:[NSURLQueryItem queryItemWithName:kLanguageQueryKey value:kLanguageQueryValue]];
    urlComponents.queryItems = [urlComponents.queryItems arrayByAddingObject:[NSURLQueryItem queryItemWithName:kUnitsQueryKey value:kUnitsQueryValue]];
    urlComponents.queryItems = [urlComponents.queryItems arrayByAddingObject:[NSURLQueryItem queryItemWithName:kApiKeyQueryKey value:self.authorizationKey]];
}

- (void)addHeadersToURLRequest:(NSMutableURLRequest *)urlRequest
{

}

- (NSObject<ADRBWFindLocationResourceProtocol> *)findLocationResource
{
    if (!_findLocationResource)
    {
        _findLocationResource = [[ADRBWFindLocationResource alloc] initWithService:self];
    }

    return _findLocationResource;
}

- (NSObject<ADRBWCurrentWeatherResourceProtocol> *)currentWeatherResource
{
    if (!_currentWeatherResource)
    {
        _currentWeatherResource = [[ADRBWCurrentWeatherResource alloc] initWithService:self];
    }

    return _currentWeatherResource;
}

- (NSObject<ADRBWForecastWeatherResourceProtocol> *)forecastWeatherResource
{
    if (!_forecastWeatherResource)
    {
        _forecastWeatherResource = [[ADRBWForecastWeatherResource alloc] initWithService:self];
    }

    return _forecastWeatherResource;
}

@end