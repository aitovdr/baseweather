//
// Created by dmitrii.aitov@philips.com on 01/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADRNetworkService.h"

@protocol ADRNetworkClientProtocol;

@protocol ADRBWFindLocationResourceProtocol;
@protocol ADRBWCurrentWeatherResourceProtocol;
@protocol ADRBWForecastWeatherResourceProtocol;


///Service which describes functionality of openweathermap.com REST API
@interface ADRBWOpenWeatherMapService : ADRNetworkService

@property (nonatomic, strong, readonly, nonnull) NSObject<ADRBWFindLocationResourceProtocol> *findLocationResource;
@property (nonatomic, strong, readonly, nonnull) NSObject<ADRBWCurrentWeatherResourceProtocol> *currentWeatherResource;
@property (nonatomic, strong, readonly, nonnull) NSObject<ADRBWForecastWeatherResourceProtocol> *forecastWeatherResource;

/// Create new instance of ADRBWOpenWeatherMapService
/// @param authorizationKey key to access openweathermap.com API
/// @param networkClient low level generic network client to execute requests
/// @return new instance of ADRBWOpenWeatherMapService
- (nullable instancetype)initWithAuthorizationKey:(nonnull NSString *)authorizationKey
                                    networkClient:(nonnull NSObject<ADRNetworkClientProtocol> *)networkClient;

///Made init and new unavailable to avoid creation without mandatory parameters
+ (nullable instancetype)new NS_UNAVAILABLE;
- (nullable instancetype)init NS_UNAVAILABLE;

@end
