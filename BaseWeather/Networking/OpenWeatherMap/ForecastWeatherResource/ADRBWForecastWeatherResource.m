//
// Created by dmitrii.aitov@philips.com on 01/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWForecastWeatherResource.h"
#import "ADRNetworkClientProtocol.h"
#import "ADRNetworkService.h"


@implementation ADRBWForecastWeatherResource

- (NSURLSessionTask *)getForecastWeatherByLocationIdentifier:(NSUInteger)identifier completion:(ADRNetworkClientCompletionBlock)completion
{
    NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:[NSURL URLWithString:@"forecast/daily" relativeToURL:self.networkService.baseURL] resolvingAgainstBaseURL:YES];
    urlComponents.queryItems = @[[NSURLQueryItem queryItemWithName:@"id" value:[NSString stringWithFormat:@"%lu", identifier]]];
    urlComponents.queryItems = [urlComponents.queryItems arrayByAddingObject:[NSURLQueryItem queryItemWithName:@"cnt" value:@"6"]];

    [self.networkService addParametersToURLComponents:urlComponents];

    NSURLRequest *request = [NSURLRequest requestWithURL:urlComponents.URL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10 * 1000];

    return [self.networkService.networkCLient executeRequest:request completion:completion];
}

@end
