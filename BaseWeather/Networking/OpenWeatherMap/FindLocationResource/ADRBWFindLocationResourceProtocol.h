//
// Created by dmitrii.aitov@philips.com on 01/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADRNetworkClientProtocol.h"

@protocol ADRBWFindLocationResourceProtocol <NSObject>

- (NSURLSessionTask *)findLocationByLatitude:(double)latitude longitude:(double)longitude completion:(ADRNetworkClientCompletionBlock)completion;

@end