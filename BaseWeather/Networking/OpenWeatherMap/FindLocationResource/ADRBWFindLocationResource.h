//
// Created by dmitrii.aitov@philips.com on 01/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADRBWFindLocationResourceProtocol.h"
#import "ADRNetworkResource.h"


@interface ADRBWFindLocationResource : ADRNetworkResource <ADRBWFindLocationResourceProtocol>
@end