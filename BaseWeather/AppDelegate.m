//
//  AppDelegate.m
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 30/05/2017.
//  Copyright © 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "AppDelegate.h"

#import "ADRBWHomeScreenViewController.h"
#import "ADRBWHomeScreenViewModel.h"
#import "ADRBWOpenWeatherMapService.h"
#import "ADRNetworkClient.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];

    self.weatherService = [[ADRBWOpenWeatherMapService alloc] initWithAuthorizationKey:@"c6e381d8c7ff98f0fee43775817cf6ad" networkClient:[[ADRNetworkClient alloc] init]];

    ADRBWHomeScreenViewModel *homeScreenViewModel = [[ADRBWHomeScreenViewModel alloc] init];
     ADRBWHomeScreenViewController *hvc = [[ADRBWHomeScreenViewController alloc] initWithViewModel:homeScreenViewModel];
    UINavigationController *rvc = [[UINavigationController alloc] initWithRootViewController:hvc];

    self.window.rootViewController = rvc;
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
