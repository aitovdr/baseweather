//
// Created by dmitrii.aitov@philips.com on 05/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ADRBWErrorDelegate;

@protocol ADRBWLocationScreenViewModelProtocol <NSObject>

@property (nonatomic, weak, readwrite, nullable) NSObject<ADRBWErrorDelegate> *errorDelegate;

@property (nonatomic, copy, readonly, nonnull) NSString *name;
@property (nonatomic, copy, readonly, nonnull) NSString *temperature;
@property (nonatomic, copy, readonly, nonnull) NSString *backgroundImageName;
@property (nonatomic, copy, readonly, nonnull) NSString *condition;
@property (nonatomic, copy, readonly, nonnull) NSString *conditionImageName;
@property (nonatomic, copy, readonly, nonnull) NSString *humidity;
@property (nonatomic, copy, readonly, nonnull) NSString *rain;
@property (nonatomic, copy, readonly, nonnull) NSString *wind;


@property (nonatomic, assign, readwrite) BOOL needUpdateView;

@end