//
// Created by dmitrii.aitov@philips.com on 05/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWHomeScreenViewModel.h"
#import <UIKit/UIKit.h>
#import "ADRBWErrorDelegate.h"
#import "ADRBWError.h"
#import "ADRBWLocationCellViewModel.h"
#import "ADRBWOpenWeatherMapService.h"
#import "ADRBWFindLocationResourceProtocol.h"
#import "ADRBWLocation.h"
#import "ADRBWLocationScreenViewModel.h"
#import "ADRBWSettings.h"
#import "ADRBWLocationBuilder.h"
#import "AppDelegate.h"


static NSString *const kLocationsFileName = @"locations.dat";

@interface ADRBWHomeScreenViewModel ()

@property (nonatomic, strong, readwrite, nonnull) NSArray<ADRBWLocation *> *locations;
@property (nonatomic, copy, readonly, nonnull) NSString *locationsFilePath;

@property (nonatomic, strong, nonnull) ADRBWLocation *selectedLocation;

@end

@implementation ADRBWHomeScreenViewModel
@synthesize errorDelegate = _errorDelegate;
@synthesize selectedCoordinate = _selectedCoordinate;
@synthesize needUpdateView = _needUpdateView;

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = paths.firstObject;
        
        _locationsFilePath = [documentsDirectory stringByAppendingPathComponent:kLocationsFileName];
        _locations = [NSKeyedUnarchiver unarchiveObjectWithFile:_locationsFilePath];
        
        if (!_locations)
        {
            _locations = [[NSArray alloc] init];
        }
    }
    
    return self;
}

- (void)setSelectedCoordinate:(CLLocationCoordinate2D)selectedCoordinate
{
    _selectedCoordinate = selectedCoordinate;

    [self findLocation];
}

- (NSString *)addLocationButtonTitle
{
    if (self.selectedLocation != nil)
    {
        return [NSString stringWithFormat:@"Add %@", self.selectedLocation.name];
    }
    else
    {
        return @"No selected location";
    }
}

- (void)findLocation
{
    [self.appDelegate.weatherService.findLocationResource findLocationByLatitude:self.selectedCoordinate.latitude longitude:self.selectedCoordinate.longitude completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error)
        {
            NSLog(@"error: %@", error);

            ADRBWError *internalError = [ADRBWError errorWithDomain:@"adr.baseweather" code:error.code userInfo:error.userInfo];
            internalError.recoverBlock = ^{
                [self findLocation];
            };
            [self.errorDelegate didFailOperationWithError:internalError];

            return;
        }

        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (httpResponse.statusCode > 200)
        {
            NSLog(@"http error with code: %ld description: %@", (long)httpResponse.statusCode, [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]);

            ADRBWError *internalError = [ADRBWError errorWithDomain:@"adr.baseweather" code:httpResponse.statusCode userInfo:nil];
            internalError.recoverBlock = ^{
                [self findLocation];
            };
            [self.errorDelegate didFailOperationWithError:internalError];

            return;
        }

        ADRBWLocation *location = [ADRBWLocationBuilder buildLocationWithData:data];

        self.selectedLocation = location;
        self.needUpdateView = YES;
    }];
}

- (void)bookmarkSelectedLocation
{
    if (self.selectedLocation == nil)
    {
        return;
    }

    if (![self.locations containsObject:self.selectedLocation])
    {
        _locations = [self.locations arrayByAddingObject:self.selectedLocation];
        [self save];
    }
}

- (void)removeLocationsAtIndexes:(nonnull NSIndexSet *)indexes
{
    NSMutableArray *mutableLocations = [self.locations mutableCopy];
    [mutableLocations removeObjectsAtIndexes:indexes];
    _locations = [mutableLocations copy];
    [self save];
}

- (void)changeUnit
{
    ADRBWUnits newUnits = ([ADRBWSettings sharedInstance].preferredUnits + 1) % 2;
    [ADRBWSettings sharedInstance].preferredUnits = newUnits;
}


- (nullable NSObject<ADRBWLocationCellViewModelProtocol> *)modelForLocationCellAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return [[ADRBWLocationCellViewModel alloc] initWithLocation:self.locations[indexPath.row]];
}

- (nullable NSObject<ADRBWLocationScreenViewModelProtocol> *)modelForLocationScreenAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return [[ADRBWLocationScreenViewModel alloc] initWithLocation:self.locations[indexPath.row]];
}

- (void)save
{
    [NSKeyedArchiver archiveRootObject:self.locations toFile:self.locationsFilePath];
}

- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

@end
