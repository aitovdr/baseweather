//
// Created by dmitrii.aitov@philips.com on 10/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWLocationScreenViewModel.h"
#import "ADRBWLocation.h"
#import "ADRBWWeather.h"
#import "AppDelegate.h"
#import "ADRBWOpenWeatherMapService.h"
#import "ADRBWForecastWeatherResourceProtocol.h"
#import "ADRBWWeatherBuilder.h"
#import "ADRBWWeatherUtils.h"
#import "ADRBWSettings.h"
#import "ADRBWError.h"
#import "ADRBWErrorDelegate.h"


@interface ADRBWLocationScreenViewModel ()

@property(nonatomic, strong, readonly, nonnull) ADRBWLocation *location;

@end

@implementation ADRBWLocationScreenViewModel
@synthesize errorDelegate = _errorDelegate;
@synthesize needUpdateView = _needUpdateView;

- (nullable instancetype)initWithLocation:(ADRBWLocation *)location
{
    self = [super init];

    if (self)
    {
        _location = location;
        [self setupObservers];
        [self updateForecastWeather];
    }


    return self;
}

- (void)dealloc
{
    [self.location removeObserver:self forKeyPath:NSStringFromSelector(@selector(weathers))];
}

- (void)setupObservers
{
    [self.location addObserver:self forKeyPath:NSStringFromSelector(@selector(weathers)) options:NSKeyValueObservingOptionNew context:nil];
}

- (void)updateForecastWeather
{
    [self.appDelegate.weatherService.forecastWeatherResource getForecastWeatherByLocationIdentifier:self.location.identifier completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error)
        {
            NSLog(@"error: %@", error);

            ADRBWError *internalError = [ADRBWError errorWithDomain:@"adr.baseweather" code:error.code userInfo:error.userInfo];
            internalError.recoverBlock = ^{
                [self updateForecastWeather];
            };
            [self.errorDelegate didFailOperationWithError:internalError];

            return;
        }

        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (httpResponse.statusCode > 200)
        {
            NSLog(@"http error with code: %ld description: %@", (long)httpResponse.statusCode, [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]);

            ADRBWError *internalError = [ADRBWError errorWithDomain:@"adr.baseweather" code:httpResponse.statusCode userInfo:nil];
            internalError.recoverBlock = ^{
                [self updateForecastWeather];
            };
            [self.errorDelegate didFailOperationWithError:internalError];

            return;
        }

        NSArray<ADRBWWeather *> *forecastWeathers = [ADRBWWeatherBuilder buildForecastWeatherWithData:data];

        [forecastWeathers enumerateObjectsUsingBlock:^(ADRBWWeather *weather, NSUInteger idx, BOOL *stop) {
            [self.location setWeather:weather toDate:[[NSDate date] dateByAddingTimeInterval:60 * 60 * 24 * (idx + 1)]];
        }];
    }];
}

- (NSString *)name
{
    return self.location.name.capitalizedString;
}

- (NSString *)temperature
{
    ADRBWWeather *currentWeather = [self.location currentWeather];

    if ([ADRBWSettings sharedInstance].preferredUnits == ADRBWUnitsMetric)
    {
        return (currentWeather != nil) ? [NSString stringWithFormat:@"%.0f°C", currentWeather.temperature] : @"- -";
    }
    else
    {
        double fahrenheitTemperature = [ADRBWWeatherUtils convertCelsiusToFahrenheit:currentWeather.temperature];
        return (currentWeather != nil) ? [NSString stringWithFormat:@"%.0f°F", fahrenheitTemperature] : @"- -";
    }
}

- (NSString *)backgroundImageName
{
    ADRBWWeather *currentWeather = [self.location currentWeather];

    return (currentWeather != nil) ? [ADRBWWeatherUtils conditionBackgroundNameByWeatherType:currentWeather.type] : nil;
}

- (NSString *)condition
{
    ADRBWWeather *currentWeather = [self.location currentWeather];

    return (currentWeather != nil) ? currentWeather.condition.capitalizedString : nil;
}

- (NSString *)conditionImageName
{
    return nil;
}

- (NSString *)humidity
{
    ADRBWWeather *currentWeather = [self.location currentWeather];

    return [NSString stringWithFormat:@"Humidity: %d%%", (int)(currentWeather.humidity)];
}

- (NSString *)rain
{
    ADRBWWeather *currentWeather = [self.location currentWeather];

    return [NSString stringWithFormat:@"Volume of rain: %d mm", (int)(currentWeather.rainVolume * 100)];
}

- (NSString *)wind
{
    ADRBWWeather *currentWeather = [self.location currentWeather];

    if ([ADRBWSettings sharedInstance].preferredUnits == ADRBWUnitsMetric)
    {
        return [NSString stringWithFormat:@"Wind: %@ %d m/s", [ADRBWWeatherUtils windDegreesToString:currentWeather.windDirection], (int)(currentWeather.wind)];
    }
    else
    {
        double milesPerHourWind = [ADRBWWeatherUtils convertMetersPerSecondToMilesPerHour:currentWeather.wind];
        return [NSString stringWithFormat:@"Wind: %@ %d mi/h", [ADRBWWeatherUtils windDegreesToString:currentWeather.windDirection], (int)(milesPerHourWind)];
    }
}

- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    self.needUpdateView = YES;
}

@end