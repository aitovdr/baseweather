//
// Created by dmitrii.aitov@philips.com on 04/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ADRBWErrorDelegate;

@protocol ADRBWLocationCellViewModelProtocol <NSObject>

@property (nonatomic, weak, readwrite, nullable) NSObject<ADRBWErrorDelegate> *errorDelegate;

@property (nonatomic, copy, readonly, nonnull) NSString *name;
@property (nonatomic, copy, readonly, nonnull) NSString *temperature;
@property (nonatomic, copy, readonly, nonnull) NSString *backgroundImageName;

@property (nonatomic, assign, readwrite) BOOL needUpdateView;

@end