//
// Created by dmitrii.aitov@philips.com on 05/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWLocationCellViewModel.h"
#import "ADRBWLocation.h"
#import "AppDelegate.h"
#import "ADRBWOpenWeatherMapService.h"
#import "ADRBWCurrentWeatherResourceProtocol.h"
#import "ADRBWWeatherBuilder.h"
#import "ADRBWWeather.h"
#import "ADRBWWeatherUtils.h"
#import "ADRBWSettings.h"
#import "ADRBWError.h"
#import "ADRBWErrorDelegate.h"

@interface ADRBWLocationCellViewModel ()

@property (nonatomic, strong, readonly, nonnull) ADRBWLocation *location;

@end

@implementation ADRBWLocationCellViewModel
@synthesize errorDelegate = _errorDelegate;
@synthesize needUpdateView = _needUpdateView;

- (nullable instancetype)initWithLocation:(ADRBWLocation *)location
{
    self = [super init];

    if (self)
    {
        _location = location;
        [self setupObservers];
        [self updateCurrentWeather];
    }

    return self;
}

- (void)dealloc
{
    [self.location removeObserver:self forKeyPath:NSStringFromSelector(@selector(weathers))];
}

- (void)setupObservers
{
    [self.location addObserver:self forKeyPath:NSStringFromSelector(@selector(weathers)) options:NSKeyValueObservingOptionNew context:nil];
}

- (void)updateCurrentWeather
{
    [self.appDelegate.weatherService.currentWeatherResource getCurrentWeatherByLocationIdentifier:self.location.identifier completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error)
        {
            NSLog(@"error: %@", error);

            ADRBWError *internalError = [ADRBWError errorWithDomain:@"adr.baseweather" code:error.code userInfo:error.userInfo];
            internalError.recoverBlock = ^{
                [self updateCurrentWeather];
            };
            [self.errorDelegate didFailOperationWithError:internalError];

            return;
        }

        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (httpResponse.statusCode > 200)
        {
            NSLog(@"http error with code: %ld description: %@", (long)httpResponse.statusCode, [NSHTTPURLResponse localizedStringForStatusCode:httpResponse.statusCode]);

            ADRBWError *internalError = [ADRBWError errorWithDomain:@"adr.baseweather" code:httpResponse.statusCode userInfo:nil];
            internalError.recoverBlock = ^{
                [self updateCurrentWeather];
            };
            [self.errorDelegate didFailOperationWithError:internalError];

            return;
        }

        ADRBWWeather *currentWeather = [ADRBWWeatherBuilder buildCurrentWeatherWithData:data];
        [self.location setWeather:currentWeather toDate:[NSDate date]];
    }];
}

- (NSString *)name
{
    return self.location.name.capitalizedString;
}

- (NSString *)temperature
{
    ADRBWWeather *currentWeather = [self.location currentWeather];

    if ([ADRBWSettings sharedInstance].preferredUnits == ADRBWUnitsMetric)
    {
        return (currentWeather != nil) ? [NSString stringWithFormat:@"%.0f°C", currentWeather.temperature] : @"- -";
    }
    else
    {
        double fahrenheitTemperature = [ADRBWWeatherUtils convertCelsiusToFahrenheit:currentWeather.temperature];
        return (currentWeather != nil) ? [NSString stringWithFormat:@"%.0f°F", fahrenheitTemperature] : @"- -";
    }
}

- (NSString *)backgroundImageName
{
    ADRBWWeather *currentWeather = [self.location currentWeather];

    return (currentWeather != nil) ? [ADRBWWeatherUtils conditionBackgroundNameByWeatherType:currentWeather.type] : nil;
}

- (AppDelegate *)appDelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    self.needUpdateView = YES;
}

@end