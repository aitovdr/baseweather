//
// Created by dmitrii.aitov@philips.com on 05/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADRBWLocationCellViewModelProtocol.h"

@class ADRBWLocation;

@interface ADRBWLocationCellViewModel : NSObject <ADRBWLocationCellViewModelProtocol>

- (nullable instancetype)initWithLocation:(nonnull ADRBWLocation *)location;

///Made init and new unavailable to avoid creation without mandatory parameters
+ (nullable instancetype)new NS_UNAVAILABLE;
- (nullable instancetype)init NS_UNAVAILABLE;

@end