//
// Created by dmitrii.aitov@philips.com on 03/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CLLocation.h>

@class ADRBWLocation;
@protocol ADRBWLocationCellViewModelProtocol;
@protocol ADRBWLocationScreenViewModelProtocol;
@protocol ADRBWErrorDelegate;

@protocol ADRBWHomeScreenViewModelProtocol <NSObject>

@property (nonatomic, weak, readwrite, nullable) NSObject<ADRBWErrorDelegate> *errorDelegate;

@property (nonatomic, strong, readonly, nonnull) NSArray<ADRBWLocation *> *locations;
@property (nonatomic, assign, readwrite) CLLocationCoordinate2D selectedCoordinate;
@property (nonatomic, copy, readonly, nonnull) NSString *addLocationButtonTitle;

@property (nonatomic, assign, readwrite) BOOL needUpdateView;

- (void)bookmarkSelectedLocation;

- (void)removeLocationsAtIndexes:(nonnull NSIndexSet *)indexes;

- (void)changeUnit;

/// View model object for product cell
/// @param indexPath index path of cell
/// @return new instance of product cell view model
- (nullable NSObject<ADRBWLocationCellViewModelProtocol> *)modelForLocationCellAtIndexPath:(nonnull NSIndexPath *)indexPath;

/// View model object for product screen
/// @param indexPath index path of product
/// @return new instance of product screen view model
- (nullable NSObject<ADRBWLocationScreenViewModelProtocol> *)modelForLocationScreenAtIndexPath:(nonnull NSIndexPath *)indexPath;


@end
