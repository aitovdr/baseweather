//
// Created by dmitrii.aitov@philips.com on 05/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADRBWHomeScreenViewModelProtocol.h"


@interface ADRBWHomeScreenViewModel : NSObject <ADRBWHomeScreenViewModelProtocol>

@end