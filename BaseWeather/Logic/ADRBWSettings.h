//
// Created by dmitrii.aitov@philips.com on 10/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * _Nonnull const ADRBWPreferredUnitsKey;

typedef NS_ENUM(NSUInteger, ADRBWUnits)
{
    ADRBWUnitsMetric,
    ADRBWUnitsImperial
};

@interface ADRBWSettings : NSObject

@property (nonatomic, assign, readwrite) ADRBWUnits preferredUnits;

+ (nullable instancetype)sharedInstance;

@end