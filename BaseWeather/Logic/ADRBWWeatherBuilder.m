//
// Created by dmitrii.aitov@philips.com on 04/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWWeatherBuilder.h"
#import "ADRBWWeather.h"
#import "ADRBWWeatherUtils.h"


@implementation ADRBWWeatherBuilder

+ (nullable ADRBWWeather *)buildCurrentWeatherWithData:(NSData *)data
{
    NSError *jsonParseError = nil;
    NSDictionary* locationDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];

    if (jsonParseError || !locationDictionary)
    {
        return nil;
    }

    return [self buildCurrentWeatherWithDictionary:locationDictionary];
}

+ (nullable ADRBWWeather *)buildCurrentWeatherWithDictionary:(NSDictionary *)locationDictionary
{
    double temperature = [locationDictionary[@"main"][@"temp"] doubleValue];
    double temperatureMin = [locationDictionary[@"main"][@"temp_min"] doubleValue];
    double temperatureMax = [locationDictionary[@"main"][@"temp_max"] doubleValue];

    NSUInteger humidity = [locationDictionary[@"main"][@"humidity"] unsignedIntegerValue];
    NSUInteger pressure = [locationDictionary[@"main"][@"pressure"] unsignedIntegerValue];

    double wind = [locationDictionary[@"wind"][@"speed"] doubleValue];
    NSUInteger windDirection = [locationDictionary[@"wind"][@"deg"] unsignedIntegerValue];

    double rainVolume = [locationDictionary[@"rain"][@"3h"] doubleValue];;

    NSString *description = locationDictionary[@"weather"][0][@"description"];
    if (description == nil)
    {
        return nil;
    }

    NSUInteger weatherCode = [locationDictionary[@"weather"][0][@"id"] unsignedIntegerValue];
    ADRBWWeatherType type = [ADRBWWeatherUtils weatherTypeByCode:weatherCode];

    return [[ADRBWWeather alloc] initWithTemperature:temperature
                                      temperatureMin:temperatureMin
                                      temperatureMax:temperatureMax
                                            humidity:humidity
                                            pressure:pressure
                                                wind:wind
                                       windDirection:windDirection
                                          rainVolume:rainVolume
                                           condition:description
                                                type:type];
}


+ (nonnull NSArray<ADRBWWeather *> *)buildForecastWeatherWithData:(nonnull NSData *)data
{
    NSError *jsonParseError = nil;
    NSDictionary *forecastDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];

    if (jsonParseError || !forecastDictionary)
    {
        return @[];
    }

    return [self buildForecastWeatherWithDictionaries:forecastDictionary[@"list"]];
}

+ (nonnull NSArray<ADRBWWeather *> *)buildForecastWeatherWithDictionaries:(nonnull NSArray<NSDictionary*> *)dictionaries
{
    NSMutableArray<ADRBWWeather *> *weathers = [NSMutableArray array];

    [dictionaries enumerateObjectsUsingBlock:^(NSDictionary *weatherDictionary, NSUInteger idx, BOOL *stop) {
        if (idx != 0)
        {
            [weathers addObject:[self buildForecastWeatherWithDictionary:weatherDictionary]];
        }
    }];

    return [weathers copy];
}

+ (nullable ADRBWWeather *)buildForecastWeatherWithDictionary:(NSDictionary *)dictionary
{
    double temperature = [dictionary[@"temp"][@"day"] doubleValue];
    double temperatureMin = [dictionary[@"temp"][@"min"] doubleValue];
    double temperatureMax = [dictionary[@"temp"][@"max"] doubleValue];

    NSUInteger humidity = [dictionary[@"humidity"] unsignedIntegerValue];
    NSUInteger pressure = [dictionary[@"pressure"] unsignedIntegerValue];

    double wind = [dictionary[@"speed"] doubleValue];
    NSUInteger windDirection = [dictionary[@"deg"] unsignedIntegerValue];

    double chancesOfRain = 0.2f;

    NSString *description = dictionary[@"weather"][0][@"description"];
    if (description == nil)
    {
        return nil;
    }

    NSUInteger weatherCode = [dictionary[@"weather"][0][@"id"] unsignedIntegerValue];
    ADRBWWeatherType type = [ADRBWWeatherUtils weatherTypeByCode:weatherCode];

    return [[ADRBWWeather alloc] initWithTemperature:temperature
                                      temperatureMin:temperatureMin
                                      temperatureMax:temperatureMax
                                            humidity:humidity
                                            pressure:pressure
                                                wind:wind
                                       windDirection:windDirection
                                          rainVolume:chancesOfRain
                                           condition:description
                                                type:type];
}

@end