//
// Created by dmitrii.aitov@philips.com on 04/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADRBWLocation;


@interface ADRBWLocationBuilder : NSObject

+ (nullable ADRBWLocation *)buildLocationWithData:(nonnull NSData *)data;

@end