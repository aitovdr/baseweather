//
// Created by dmitrii.aitov@philips.com on 04/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ADRBWWeather;


@interface ADRBWWeatherBuilder : NSObject

+ (nullable ADRBWWeather *)buildCurrentWeatherWithData:(nonnull NSData *)data;

+ (nonnull NSArray<ADRBWWeather *> *)buildForecastWeatherWithData:(nonnull NSData *)data;

@end