//
// Created by dmitrii.aitov@philips.com on 04/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWLocationBuilder.h"
#import "ADRBWLocation.h"


@implementation ADRBWLocationBuilder

+ (nullable ADRBWLocation *)buildLocationWithData:(NSData *)data
{
    NSError *jsonParseError = nil;
    NSDictionary* locationDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParseError];

    if (jsonParseError || !locationDictionary)
    {
        return nil;
    }

    return [self buildLocationWithDictionary:locationDictionary];
}

+ (nullable ADRBWLocation *)buildLocationWithDictionary:(NSDictionary *)locationDictionary
{
    NSString *name = locationDictionary[@"name"];
    if (name == nil)
    {
        return nil;
    }

    NSString *country = locationDictionary[@"sys"][@"country"];
    if (country == nil)
    {
        return nil;
    }


    NSUInteger identifier = [locationDictionary[@"id"] unsignedIntegerValue];

    return [[ADRBWLocation alloc] initWithName:name country:country identifier:identifier];
}

@end