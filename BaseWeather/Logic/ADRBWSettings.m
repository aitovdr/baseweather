//
// Created by dmitrii.aitov@philips.com on 10/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWSettings.h"

NSString * _Nonnull const ADRBWPreferredUnitsKey = @"ADRBWPreferredUnitsKey";

@implementation ADRBWSettings

+ (instancetype)sharedInstance {
    static ADRBWSettings *_sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[ADRBWSettings alloc] init];
    });

    return _sharedInstance;
}

- (ADRBWUnits)preferredUnits
{
    ADRBWUnits defaultUnits = ([NSLocale currentLocale].usesMetricSystem) ? ADRBWUnitsMetric : ADRBWUnitsImperial;
    return (ADRBWUnits)[self integerForKey:ADRBWPreferredUnitsKey defaultValue:defaultUnits];
}

- (void)setPreferredUnits:(ADRBWUnits)preferredUnits
{
    [self putIntegerForKey:ADRBWPreferredUnitsKey value:preferredUnits];
}

#pragma mark - Private helpers
- (NSUserDefaults *)userDefaults
{
    return [NSUserDefaults standardUserDefaults];
}

- (BOOL)contains:(NSString*)key
{
    return [[self userDefaults] valueForKey:key] != nil;
}

- (NSInteger)integerForKey:(NSString*)key defaultValue:(NSInteger)defaultValue
{
    return [self contains:key] ? [[self userDefaults] integerForKey : key] : defaultValue;
}

- (NSInteger)integerForKey:(NSString*)key
{
    NSParameterAssert([self contains:key]);
    return [[self userDefaults] integerForKey:key];
}

- (void)putIntegerForKey:(NSString*)key value:(NSInteger)value
{
    NSUserDefaults* defaults = [self userDefaults];
    [defaults setInteger:value forKey:key];
    [defaults synchronize];
}

@end