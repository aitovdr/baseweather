//
// Created by dmitrii.aitov@philips.com on 10/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "NSDate+Utils.h"

static const unsigned componentFlags = (NSCalendarUnitYear| NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekOfYear |  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday | NSCalendarUnitWeekdayOrdinal);

@implementation NSDate (Utils)

+ (NSCalendar *)currentCalendar
{
    static NSCalendar *sharedCalendar = nil;
    if (!sharedCalendar)
        sharedCalendar = [NSCalendar autoupdatingCurrentCalendar];
    return sharedCalendar;
}

- (NSDate *)dateAtStartOfDay
{
    return [self dateAtStartOfDayFor:[NSDate currentCalendar]];
}

- (NSDate *)dateAtStartOfDayFor:(NSCalendar*)calendar
{
    NSDateComponents *components = [calendar components:componentFlags fromDate:self];
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
    return [calendar dateFromComponents:components];
}

- (NSDate *)dateAtEndOfDay
{
    return [self dateAtEndOfDayFor:[NSDate currentCalendar]];
}

- (NSDate *)dateAtEndOfDayFor:(NSCalendar*)calendar
{
    NSDateComponents *components = [calendar components:componentFlags fromDate:self];
    components.hour = 23;
    components.minute = 59;
    components.second = 59;
    return [calendar dateFromComponents:components];
}


@end