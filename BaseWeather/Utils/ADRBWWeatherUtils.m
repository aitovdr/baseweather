//
// Created by dmitrii.aitov@philips.com on 10/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import "ADRBWWeatherUtils.h"


@implementation ADRBWWeatherUtils

+ (ADRBWWeatherType)weatherTypeByCode:(NSUInteger)code
{
    switch (code)
    {
        case 800:
            return ADRBWWeatherTypeClearSky;
        case 801:
            return ADRBWWeatherTypeFewClouds;
        case 802:
            return ADRBWWeatherTypeScatteredClouds;
        case 803:
        case 804:
            return ADRBWWeatherTypeBrokenClouds;
        default:
            break;
    }

    code = code / 100;

    switch (code)
    {
        case 2:
            return ADRBWWeatherTypeThunderstorm;
        case 3:
            return ADRBWWeatherTypeShowerRain;
        case 5:
            return ADRBWWeatherTypeRain;
        case 6:
            return ADRBWWeatherTypeSnow;
        case 7:
            return ADRBWWeatherTypeMist;
        default:
            return ADRBWWeatherTypeClearSky;
    }
}

+ (NSString *)conditionBackgroundNameByWeatherType:(ADRBWWeatherType)type
{
    return [self nameByWeatherType:type];
}

+ (NSString *)conditionBackgroundBigNameByWeatherType:(ADRBWWeatherType)type
{
    return [[self nameByWeatherType:type] stringByAppendingString:@"Big"];
}

+ (NSString *)conditionIconNameByWeatherType:(ADRBWWeatherType)type
{
    return [[self nameByWeatherType:type] stringByAppendingString:@"Icon"];
}

+ (NSString *)nameByWeatherType:(ADRBWWeatherType)type
{
    switch (type)
    {
        case ADRBWWeatherTypeClearSky:
        {
            return @"clearSky";
        }
        case ADRBWWeatherTypeFewClouds:
        {
            return @"fewClouds";
        }
        case ADRBWWeatherTypeScatteredClouds:
        {
            return @"scatteredClouds";
        }
        case ADRBWWeatherTypeBrokenClouds:
        {
            return @"brokenClouds";
        }
        case ADRBWWeatherTypeShowerRain:
        {
            return @"showerRain";
        }
        case ADRBWWeatherTypeRain:
        {
            return @"rain";
        }
        case ADRBWWeatherTypeThunderstorm:
        {
            return @"thunderstorm";
        }
        case ADRBWWeatherTypeSnow:
        {
            return @"snow";
        }
        case ADRBWWeatherTypeMist:
        {
            return @"mist";
        }
        default:
            return nil;
    }
}

+ (NSString *)windDegreesToString:(NSUInteger)degrees
{
    return @[@"N", @"NNE", @"NE", @"ENE", @"E", @"ESE",  @"SE",  @"SSE", @"S", @"SSW", @"SW", @"WSW", @"W", @"WNW", @"NW", @"NNW"][((NSUInteger)((degrees / 22.5f) + 0.5f) % 16)];
}

+ (double)convertCelsiusToFahrenheit:(double)celsius
{
    return [self roundDouble:celsius * 1.8 + 32 toPlaces:2];
}

+ (double)convertMetersPerSecondToMilesPerHour:(double)metersPerSecond
{
    return [self roundDouble:metersPerSecond * 2.2369 toPlaces:2];
}

+ (double)roundDouble:(double)number toPlaces:(NSUInteger)places
{
    double divisor = pow(10.0, places);
    return round(number * divisor) / divisor;
}

@end
