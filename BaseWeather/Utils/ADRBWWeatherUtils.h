//
// Created by dmitrii.aitov@philips.com on 10/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADRBWWeather.h"


@interface ADRBWWeatherUtils : NSObject

+ (ADRBWWeatherType)weatherTypeByCode:(NSUInteger)code;

+ (NSString *)conditionBackgroundNameByWeatherType:(ADRBWWeatherType)type;
+ (NSString *)conditionBackgroundBigNameByWeatherType:(ADRBWWeatherType)type;
+ (NSString *)conditionIconNameByWeatherType:(ADRBWWeatherType)type;

+ (NSString *)windDegreesToString:(NSUInteger)degrees;

+ (double)convertCelsiusToFahrenheit:(double)celsius;

+ (double)convertMetersPerSecondToMilesPerHour:(double)metersPerSecond;

@end