//
// Created by dmitrii.aitov@philips.com on 10/06/2017.
// Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utils)

- (NSDate *)dateAtStartOfDay;
- (NSDate *)dateAtEndOfDay;

@end