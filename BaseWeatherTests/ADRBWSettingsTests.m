//
//  ADRBWSettingsTests.m
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 11/06/2017.
//  Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ADRBWSettings.h"

@interface ADRBWSettingsTests : XCTestCase

@end

@implementation ADRBWSettingsTests

- (void)setUp {
    [super setUp];

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:ADRBWPreferredUnitsKey];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testDefaultPreferredUnits
{
    ADRBWUnits expectedUnits = ([NSLocale currentLocale].usesMetricSystem) ? ADRBWUnitsMetric : ADRBWUnitsImperial;

    ADRBWSettings *settings = [ADRBWSettings sharedInstance];

    XCTAssertNotNil(settings, "Settings should be not nil");

    XCTAssertEqual(expectedUnits, settings.preferredUnits, @"Units systems should be equal");
}

- (void)testChangePreferredUnits
{
    ADRBWUnits expectedUnits = ([NSLocale currentLocale].usesMetricSystem) ? ADRBWUnitsMetric : ADRBWUnitsImperial;

    ADRBWSettings *settings = [ADRBWSettings sharedInstance];

    XCTAssertNotNil(settings, "Settings should be not nil");

    XCTAssertEqual(expectedUnits, settings.preferredUnits, @"Units systems should be equal");

    ADRBWUnits newUnits = (expectedUnits + 1) % 2;
    [ADRBWSettings sharedInstance].preferredUnits = newUnits;

    XCTAssertEqual(newUnits, [ADRBWSettings sharedInstance].preferredUnits, @"Units systems should be equal");
}

@end
