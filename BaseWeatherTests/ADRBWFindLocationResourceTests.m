//
//  ADRBWFindLocationResourceTests.m
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 11/06/2017.
//  Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ADRBWOpenWeatherMapService.h"
#import "ADRBWFindLocationResource.h"
#import "ADRNetworkClient.h"

static NSString *const kOWMAPIKey = @"c6e381d8c7ff98f0fee43775817cf6ad";

@interface ADRBWFindLocationResourceTests : XCTestCase

@property (nonatomic, strong, readwrite, nonnull) ADRBWOpenWeatherMapService *owmService;
@property (nonatomic, strong, readwrite, nonnull) ADRBWFindLocationResource *findLocationResource;

@end

@implementation ADRBWFindLocationResourceTests

- (void)setUp
{
    [super setUp];
    
    self.owmService = [[ADRBWOpenWeatherMapService alloc] initWithAuthorizationKey:kOWMAPIKey networkClient:[[ADRNetworkClient alloc] init]];
    self.findLocationResource = (ADRBWFindLocationResource *)self.owmService.findLocationResource;
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testFindLocation
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"find"];

    NSURLSessionTask *task = [self.findLocationResource findLocationByLatitude:52.37 longitude:4.89 completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        XCTAssertNotNil(data, "data should not be nil");
        XCTAssertNil(error, "error should be nil");

        if ([response isKindOfClass:[NSHTTPURLResponse class]])
        {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            XCTAssertEqual(httpResponse.statusCode, 200, @"HTTP response status code should be 200");
        }
        else
        {
            XCTFail(@"Response was not NSHTTPURLResponse");
        }

        [expectation fulfill];
    }];

    [self waitForExpectationsWithTimeout:task.originalRequest.timeoutInterval handler:^(NSError *error) {
        if (error != nil)
        {
            NSLog(@"Error: %@", error.localizedDescription);
        }
        [task cancel];
    }];
}

@end
