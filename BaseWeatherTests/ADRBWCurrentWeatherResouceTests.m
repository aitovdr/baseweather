//
//  ADRBWCurrentWeatherResouceTests.m
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 11/06/2017.
//  Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ADRBWOpenWeatherMapService.h"
#import "ADRBWCurrentWeatherResource.h"
#import "ADRNetworkClient.h"

static NSString *const kOWMAPIKey = @"c6e381d8c7ff98f0fee43775817cf6ad";

@interface ADRBWCurrentWeatherResouceTests : XCTestCase

@property (nonatomic, strong, readwrite, nonnull) ADRBWOpenWeatherMapService *owmService;
@property (nonatomic, strong, readwrite, nonnull) ADRBWCurrentWeatherResource *curentWeatherResource;

@end

@implementation ADRBWCurrentWeatherResouceTests

- (void)setUp
{
    [super setUp];

    self.owmService = [[ADRBWOpenWeatherMapService alloc] initWithAuthorizationKey:kOWMAPIKey networkClient:[[ADRNetworkClient alloc] init]];
    self.curentWeatherResource = (ADRBWCurrentWeatherResource *)self.owmService.currentWeatherResource;
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testFindLocation
{
    XCTestExpectation *expectation = [self expectationWithDescription:@"currentWeather"];

    NSURLSessionTask *task = [self.curentWeatherResource getCurrentWeatherByLocationIdentifier:2172797 completion:^(NSData *data, NSURLResponse *response, NSError *error) {
        XCTAssertNotNil(data, "data should not be nil");
        XCTAssertNil(error, "error should be nil");

        if ([response isKindOfClass:[NSHTTPURLResponse class]])
        {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            XCTAssertEqual(httpResponse.statusCode, 200, @"HTTP response status code should be 200");
        }
        else
        {
            XCTFail(@"Response was not NSHTTPURLResponse");
        }

        [expectation fulfill];
    }];

    [self waitForExpectationsWithTimeout:task.originalRequest.timeoutInterval handler:^(NSError *error) {
        if (error != nil)
        {
            NSLog(@"Error: %@", error.localizedDescription);
        }
        [task cancel];
    }];
}

@end
