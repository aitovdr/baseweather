//
//  ADRBWHomeScreenViewModelTests.m
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 11/06/2017.
//  Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ADRBWHomeScreenViewModel.h"
#import "ADRBWLocationCellViewModel.h"
#import "ADRBWLocationScreenViewModel.h"
#import "ADRBWLocation.h"
#import "ADRBWWeather.h"
#import "ADRBWSettings.h"
#import "ADRBWWeatherUtils.h"


static NSString *const kLocationsFileName = @"locations.dat";

@interface ADRBWHomeScreenViewModelTests : XCTestCase

@property (nonatomic, strong, readonly, nonnull) ADRBWHomeScreenViewModel *underTest;
@property (nonatomic, strong, readonly, nonnull) XCTestExpectation *expectation;
@property (nonatomic, strong, readwrite, nullable) XCTestExpectation *cellExpectation;

@end

@implementation ADRBWHomeScreenViewModelTests

- (void)setUp {
    [super setUp];
    [self.class cleanBookmarkedLocations];
    [ADRBWSettings sharedInstance].preferredUnits = ADRBWUnitsMetric;
    _underTest = [[ADRBWHomeScreenViewModel alloc] init];
    _expectation = [self expectationWithDescription:@"find"];
    _cellExpectation = nil;
}

- (void)tearDown {
    [self.class cleanBookmarkedLocations];
    [super tearDown];
}

- (void)testBookmarkLocations
{
    XCTAssertEqual(self.underTest.locations.count, 0, @"Should be 0 bookmarked locations at this point");

    [self.underTest addObserver:self forKeyPath:NSStringFromSelector(@selector(needUpdateView)) options:NSKeyValueObservingOptionNew context:nil];
    self.underTest.selectedCoordinate = CLLocationCoordinate2DMake(52.37, 4.89);

    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
        if (error != nil)
        {
            XCTFail(@"Location not found");
            NSLog(@"Error: %@", error.localizedDescription);
        }
        [self.underTest removeObserver:self forKeyPath:NSStringFromSelector(@selector(needUpdateView))];
    }];

    XCTAssertEqual(self.underTest.locations.count, 0, @"Should be 0 bookmarked locations at this point");
    XCTAssertEqualObjects(self.underTest.addLocationButtonTitle, @"Add Amsterdam", @"Should be equal to Add Amsterdam");
    
    [self.underTest bookmarkSelectedLocation];
    
    XCTAssertEqual(self.underTest.locations.count, 1, @"Should be 1 bookmarked locations at this point");
    XCTAssertEqual(self.underTest.locations.firstObject.identifier, 2759794, @"First location identifier should be equal to identifier of Amsterdam(2759794)");
    
    ADRBWHomeScreenViewModel *enotherModel = [[ADRBWHomeScreenViewModel alloc] init];
    
    XCTAssertEqual(enotherModel.locations.count, 1, @"Should be 1 bookmarked locations at this point even for different view model");
    XCTAssertEqual(enotherModel.locations.firstObject.identifier, 2759794, @"First location identifier should be equal to identifier of Amsterdam(2759794)");
}

- (void)testLocationCellAndLocationScreenViewModelAtIndex
{
    XCTAssertEqual(self.underTest.locations.count, 0, @"Should be 0 bookmarked locations at this point");
    
    [self.underTest addObserver:self forKeyPath:NSStringFromSelector(@selector(needUpdateView)) options:NSKeyValueObservingOptionNew context:nil];
    self.underTest.selectedCoordinate = CLLocationCoordinate2DMake(52.37, 4.89);
    
    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
        if (error != nil)
        {
            XCTFail(@"Location not found");
            NSLog(@"Error: %@", error.localizedDescription);
        }
        [self.underTest removeObserver:self forKeyPath:NSStringFromSelector(@selector(needUpdateView))];
    }];
    
    XCTAssertEqual(self.underTest.locations.count, 0, @"Should be 0 bookmarked locations at this point");

    [self.underTest bookmarkSelectedLocation];
    
    ADRBWLocation *location = self.underTest.locations.firstObject;
    
    NSObject<ADRBWLocationCellViewModelProtocol> *cellViewModel = [self.underTest modelForLocationCellAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    //NSObject<ADRBWLocationScreenViewModelProtocol> *locationScreenViewModel = [self.underTest modelForLocationScreenAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    XCTAssertNotNil(cellViewModel, @"Cell view model shouldn't be nil");
    XCTAssertEqualObjects(cellViewModel.name, location.name, @"Names should be equal");
    //XCTAssertNotNil(locationScreenViewModel, @"Location screen view model shouldn't be nil");
    //XCTAssertEqualObjects(locationScreenViewModel.name, location.name, @"Names should be equal");
    
    XCTAssertEqualObjects(cellViewModel.temperature, @"- -");
    
    XCTAssertNil(cellViewModel.backgroundImageName, @"Image names should be nil");
    
    [cellViewModel addObserver:self forKeyPath:NSStringFromSelector(@selector(needUpdateView)) options:NSKeyValueObservingOptionNew context:nil];
    _cellExpectation = [self expectationWithDescription:@"cell"];
    
    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
        if (error != nil)
        {
            XCTFail(@"Current weather not updated");
            NSLog(@"Error: %@", error.localizedDescription);
        }
        [cellViewModel removeObserver:self forKeyPath:NSStringFromSelector(@selector(needUpdateView))];
    }];
    
    NSString *temp = [NSString stringWithFormat:@"%.0f", location.currentWeather.temperature];
    XCTAssertTrue([cellViewModel.temperature containsString:temp]);
    //XCTAssertTrue([locationScreenViewModel.temperature containsString:temp]);
    
    NSString *imageName = [ADRBWWeatherUtils conditionBackgroundNameByWeatherType:location.currentWeather.type];
    XCTAssertEqualObjects(cellViewModel.backgroundImageName, imageName, @"Image names should be equal");
    //XCTAssertEqualObjects(locationScreenViewModel.backgroundImageName, imageName, @"Image names should be equal");
}

+ (void)cleanBookmarkedLocations
{
    NSError *error;
    NSString *historyFilePath = [self bookmarkedLocationsFilePath];
    [NSFileManager.defaultManager removeItemAtPath:historyFilePath error:&error];
    if (error)
    {
        NSLog(@"Error: %@", error.localizedDescription);
    }
}

+ (NSString *)bookmarkedLocationsFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths.firstObject;

    return [documentsDirectory stringByAppendingPathComponent:kLocationsFileName];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == self.underTest && self.underTest.needUpdateView)
    {
        [self.expectation fulfill];
        self.underTest.needUpdateView = NO;
    }
    else
    {
        [self.cellExpectation fulfill];
        self.cellExpectation = nil;
    }
}

@end
