//
//  ADRBWLocationBuilderTests.m
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 11/06/2017.
//  Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ADRBWLocationBuilder.h"
#import "ADRBWLocation.h"

@interface ADRBWLocationBuilderTests : XCTestCase

@end

@implementation ADRBWLocationBuilderTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testLocationCreationFromJSONData
{
    NSData *jsonData = [self createRightJsonData];

    XCTAssertNotNil(jsonData, @"Should be not nil");

    ADRBWLocation *location = [ADRBWLocationBuilder buildLocationWithData:jsonData];

    XCTAssertNotNil(location, @"Location should not be nil");
    XCTAssertTrue([location isMemberOfClass:[ADRBWLocation class]], @"Should be member of class ADRBWWeather");


    jsonData = [self createWrongJsonData];

    XCTAssertNotNil(jsonData, @"Should be not nil");

    location = [ADRBWLocationBuilder buildLocationWithData:jsonData];

    XCTAssertNil(location, @"Location should be nil");
}

- (void)testNotJSONData
{
    NSData *data = [@"Not json data" dataUsingEncoding:NSUTF8StringEncoding];

    ADRBWLocation *location = [ADRBWLocationBuilder buildLocationWithData:data];

    XCTAssertNil(location, @"Should be nil");
}

- (NSData *)createRightJsonData
{
    NSString *jsonString = @"{\"coord\":{\"lon\":145.77,\"lat\":-16.92},\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03n\"}],\"base\":\"stations\",\"main\":{\"temp\":300.15,\"pressure\":1007,\"humidity\":74,\"temp_min\":300.15,\"temp_max\":300.15},\"visibility\":10000,\"wind\":{\"speed\":3.6,\"deg\":160},\"clouds\":{\"all\":40},\"dt\":1485790200,\"sys\":{\"type\":1,\"id\":8166,\"message\":0.2064,\"country\":\"AU\",\"sunrise\":1485720272,\"sunset\":1485766550},\"id\":2172797,\"name\":\"Cairns\",\"cod\":200}";
    return [jsonString dataUsingEncoding:NSUTF8StringEncoding];
}

- (NSData *)createWrongJsonData
{
    NSString *jsonString = @"{\"coord\":{\"lon\":145.77,\"lat\":-16.92}";
    return [jsonString dataUsingEncoding:NSUTF8StringEncoding];
}

@end
