//
//  ADRBWOpenWeatherMapServiceTests.m
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 11/06/2017.
//  Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ADRNetworkClient.h"
#import "ADRBWOpenWeatherMapService.h"

static NSString *const kApiKeyQueryKey = @"appid";
static NSString *const kFormatQueryKey = @"mode";
static NSString *const kLanguageQueryKey = @"lang";
static NSString *const kUnitsQueryKey = @"units";

static NSString *const kLanguageQueryValue = @"en";
static NSString *const kFormatQueryValue = @"json";
static NSString *const kUnitsQueryValue = @"metric";

@interface ADRBWOpenWeatherMapServiceTests : XCTestCase

@end

@implementation ADRBWOpenWeatherMapServiceTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testOWMServiceCreation
{
    ADRNetworkClient *networkClient = [[ADRNetworkClient alloc] init];
    NSString *apiKey = @"veryverylongapikey";

    ADRBWOpenWeatherMapService *openWeatherMapService = [[ADRBWOpenWeatherMapService alloc] initWithAuthorizationKey:apiKey networkClient:networkClient];

    XCTAssertNotNil(openWeatherMapService, @"Should not be nil");

    XCTAssertEqualObjects(openWeatherMapService.baseURL, [NSURL URLWithString:@"http://api.openweathermap.org/data/2.5/"], @"Base url should point to OWM rest api endpoint");

    XCTAssertTrue(openWeatherMapService.networkCLient == networkClient, @"Should be same object");
}

- (void)testAddingQueryParameters
{
    ADRNetworkClient *networkClient = [[ADRNetworkClient alloc] init];
    NSString *apiKey = @"veryverylongapikey";

    ADRBWOpenWeatherMapService *openWeatherMapService = [[ADRBWOpenWeatherMapService alloc] initWithAuthorizationKey:apiKey networkClient:networkClient];

    XCTAssertNotNil(openWeatherMapService, @"Should not be nil");

    NSURLComponents *components = [[NSURLComponents alloc] initWithURL:openWeatherMapService.baseURL resolvingAgainstBaseURL:YES];

    XCTAssertNil(components.queryItems, @"At this point query should be empty");

    components.queryItems = @[];

    [openWeatherMapService addParametersToURLComponents:components];

    XCTAssertEqual(components.queryItems.count, 4, @"At this point query should 4 elements");

    XCTAssertEqualObjects(components.queryItems[0].name, kFormatQueryKey, @"First query parameter name should be format");
    XCTAssertEqualObjects(components.queryItems[0].value, kFormatQueryValue, @"First query parameter value should be json");

    XCTAssertEqualObjects(components.queryItems[1].name, kLanguageQueryKey, @"Second query parameter name should be lang");
    XCTAssertEqualObjects(components.queryItems[1].value, kLanguageQueryValue, @"Second query parameter value should be en");

    XCTAssertEqualObjects(components.queryItems[2].name, kUnitsQueryKey, @"Third query parameter name should be units");
    XCTAssertEqualObjects(components.queryItems[2].value, kUnitsQueryValue, @"Third query parameter value should be metric");

    XCTAssertEqualObjects(components.queryItems[3].name, kApiKeyQueryKey, @"Forth query parameter name should be appid");
    XCTAssertEqualObjects(components.queryItems[3].value, apiKey, @"Forth query parameter value should be veryverylongapikey");
}

- (void)testAddingHTTPHeaders
{
    ADRNetworkClient *networkClient = [[ADRNetworkClient alloc] init];
    NSString *apiKey = @"veryverylongapikey";

    ADRBWOpenWeatherMapService *openWeatherMapService = [[ADRBWOpenWeatherMapService alloc] initWithAuthorizationKey:apiKey networkClient:networkClient];

    XCTAssertNotNil(openWeatherMapService, @"Should not be nil");

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:openWeatherMapService.baseURL];

    XCTAssertNil(request.allHTTPHeaderFields, @"At this point headers should be empty");

    [openWeatherMapService addHeadersToURLRequest:request];

    XCTAssertNil(request.allHTTPHeaderFields, @"At this point headers should be empty");
}


@end
