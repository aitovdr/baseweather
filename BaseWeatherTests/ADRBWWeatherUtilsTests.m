//
//  ADRBWWeatherUtilsTests.m
//  BaseWeather
//
//  Created by dmitrii.aitov@philips.com on 11/06/2017.
//  Copyright (c) 2017 dmitrii.aitov@philips.com. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ADRBWWeatherUtils.h"

@interface ADRBWWeatherUtilsTests : XCTestCase

@end

@implementation ADRBWWeatherUtilsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

//N 348.75 - 11.25
//NNE 11.25 - 33.75
//NE 33.75 - 56.25
//ENE 56.25 - 78.75
//E 78.75 - 101.25
//ESE 101.25 - 123.75
//SE 123.75 - 146.25
//SSE 146.25 - 168.75
//S 168.75 - 191.25
//SSW 191.25 - 213.75
//SW 213.75 - 236.25
//WSW 236.25 - 258.75
//W 258.75 - 281.25
//WNW 281.25 - 303.75
//NW 303.75 - 326.25
//NNW 326.25 - 348.75

- (void)testConversionOfWindDegreesToString
{
    NSArray<NSNumber *> *angles = @[@10, @87, @113, @220, @145, @245, @300, @332, @360, @480];
    NSArray *expectedResults = @[@"N", @"E", @"ESE", @"SW", @"SE", @"WSW", @"WNW", @"NNW", @"N", @"ESE"];

    [angles enumerateObjectsUsingBlock:^(NSNumber *angle, NSUInteger idx, BOOL *stop) {
        XCTAssertEqualObjects([ADRBWWeatherUtils windDegreesToString:angle.unsignedIntegerValue], expectedResults[idx], @"Results should be equal");
    }];
}

- (void)testConversionCelsiusToFahrenheit
{
    NSArray<NSNumber *> *temperatures = @[@-100.0, @-22.0, @0, @13.0, @28.0, @245.0];
    NSArray *expectedResults = @[@-148.0, @-7.6, @32, @55.4, @82.4, @473];

    [temperatures enumerateObjectsUsingBlock:^(NSNumber *temperature, NSUInteger idx, BOOL *stop) {
        XCTAssertEqual([ADRBWWeatherUtils convertCelsiusToFahrenheit:temperature.doubleValue], [expectedResults[idx] doubleValue], @"Results should be equal");
    }];
}

- (void)testConversionMPStoMiPH
{
    NSArray<NSNumber *> *metersPerSecondArray = @[@0.0, @2.0, @8, @13.0, @-8.0, @245.0];
    NSArray *expectedResults = @[@0, @4.47, @17.9, @29.08, @-17.9, @548.04];
    
    [metersPerSecondArray enumerateObjectsUsingBlock:^(NSNumber *metersPerSecond, NSUInteger idx, BOOL *stop) {
        XCTAssertEqual([ADRBWWeatherUtils convertMetersPerSecondToMilesPerHour:metersPerSecond.doubleValue], [expectedResults[idx] doubleValue], @"Results should be equal");
    }];
}

- (void)testImageNames
{
    NSArray<NSNumber *> *weatherTypes = @[@(ADRBWWeatherTypeClearSky), @(ADRBWWeatherTypeFewClouds), @(ADRBWWeatherTypeScatteredClouds)];
    NSArray *expectedResults = @[@"clearSky", @"fewClouds", @"scatteredClouds"];

    [weatherTypes enumerateObjectsUsingBlock:^(NSNumber *weatherType, NSUInteger idx, BOOL *stop) {
        XCTAssertEqualObjects([ADRBWWeatherUtils conditionBackgroundNameByWeatherType:(ADRBWWeatherType)weatherType.unsignedIntegerValue], expectedResults[idx], @"Results should be equal");
        XCTAssertEqualObjects([ADRBWWeatherUtils conditionBackgroundBigNameByWeatherType:(ADRBWWeatherType)weatherType.unsignedIntegerValue], [expectedResults[idx] stringByAppendingString:@"Big"], @"Results should be equal");
        XCTAssertEqualObjects([ADRBWWeatherUtils conditionIconNameByWeatherType:(ADRBWWeatherType)weatherType.unsignedIntegerValue], [expectedResults[idx] stringByAppendingString:@"Icon"], @"Results should be equal");
    }];
}

- (void)testWeatherTypeByCode
{
    NSArray<NSNumber *> *weatherCodes = @[@800, @801, @802, @803, @804, @200, @205, @300, @500];
    NSArray *expectedResults = @[
            @(ADRBWWeatherTypeClearSky),
            @(ADRBWWeatherTypeFewClouds),
            @(ADRBWWeatherTypeScatteredClouds),
            @(ADRBWWeatherTypeBrokenClouds),
            @(ADRBWWeatherTypeBrokenClouds),
            @(ADRBWWeatherTypeThunderstorm),
            @(ADRBWWeatherTypeThunderstorm),
            @(ADRBWWeatherTypeShowerRain),
            @(ADRBWWeatherTypeRain)
    ];

    [weatherCodes enumerateObjectsUsingBlock:^(NSNumber *weatherCode, NSUInteger idx, BOOL *stop) {
        XCTAssertEqual([ADRBWWeatherUtils weatherTypeByCode:weatherCode.unsignedIntegerValue], (ADRBWWeatherType)[expectedResults[idx] unsignedIntegerValue], @"Results should be equal");
    }];
}

@end
